;; (import (scheme base)
;;         (scheme file))

(define-record-type lutin
  (make-lutin number calorie)
  lutin?
  (number lutin-number)
  (calorie lutin-calorie))

(call-with-input-file
  "1-input.txt"
  (lambda (port)
    (define (loop biggest-lutin current-lutin)
      (let [(line (read-line port))]
        (cond
          [(eof-object? line)
           (if (< (lutin-calorie biggest-lutin)
                  (lutin-calorie current-lutin))
             (display current-lutin)
             (display biggest-lutin))]
           


          [(equal? line "")
           (begin
             (display biggest-lutin)
             (newline)

             (let [(new-lutin (make-lutin
                                (+ (lutin-number current-lutin) 1)
                                0))]
               (if (< (lutin-calorie biggest-lutin)
                      (lutin-calorie current-lutin))
                 (loop current-lutin new-lutin)
                 (loop biggest-lutin new-lutin))))]


          [else (loop
                  biggest-lutin
                  (make-lutin (lutin-number current-lutin)
                              (+ (lutin-calorie current-lutin) (string->number line))))])))

    (loop (make-lutin 0 0) (make-lutin 1 0))))


