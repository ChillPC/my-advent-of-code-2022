(define (point-winned opponent-shape my-shape)
  (cond
    [(char=? my-shape #\X)
     (+ 1 (cond
            [(char=? opponent-shape #\A) 3]
            [(char=? opponent-shape #\B) 0]
            [(char=? opponent-shape #\C) 6]))]

    [(char=? my-shape #\Y)
     (+ 2  (cond
             [(char=? opponent-shape #\A) 6]
             [(char=? opponent-shape #\B) 3]
             [(char=? opponent-shape #\C) 0]))]

    [(char=? my-shape #\Z)
     (+ 3 (cond
            [(char=? opponent-shape #\A) 0]
            [(char=? opponent-shape #\B) 6]
            [(char=? opponent-shape #\C) 3]))]))


(call-with-input-file
  "2-input.txt"
  (lambda (port)
    (let loop [(score 0)]
      (let [(line (read-line port))]
        (if (eof-object? line)
          (display score)

          (let* [(list-line (string->list line))
                 (opponent-shape (car list-line))
                 (my-shape (caddr list-line))]
            (loop (+ score (point-winned opponent-shape my-shape)))))))))

