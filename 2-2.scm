(define (point-winned opponent-shape my-shape)
  (cond
    [(char=? my-shape #\X)
     (+ 0 (cond
            [(char=? opponent-shape #\A) 3]
            [(char=? opponent-shape #\B) 1]
            [(char=? opponent-shape #\C) 2]))]

    [(char=? my-shape #\Y)
     (+ 3  (cond
             [(char=? opponent-shape #\A) 1]
             [(char=? opponent-shape #\B) 2]
             [(char=? opponent-shape #\C) 3]))]

    [(char=? my-shape #\Z)
     (+ 6 (cond
            [(char=? opponent-shape #\A) 2]
            [(char=? opponent-shape #\B) 3]
            [(char=? opponent-shape #\C) 1]))]))


(call-with-input-file
  "2-input.txt"
  (lambda (port)
    (let loop [(score 0)]
      (let [(line (read-line port))]
        (if (eof-object? line)
          (display score)

          (let* [(list-line (string->list line))
                 (opponent-shape (car list-line))
                 (my-shape (caddr list-line))]
            (loop (+ score (point-winned opponent-shape my-shape)))))))))

