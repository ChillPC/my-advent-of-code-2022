(define (char-value char)
  (let [(ascii-value (char->integer char))]
    (cond
      [(and (<= 97 ascii-value) (<= ascii-value 122))
       (- ascii-value 96)]
      [(and (<= 65 ascii-value) (<= ascii-value 90))
       (- (+ 27 ascii-value) 65)]
      [else (error "Unwanted number")])))

(define (is-char-in-list char char-list)
  (cond
    [(null? char-list) #f]
    [(char=? char (car char-list)) #t]
    [else (is-char-in-list char (cdr char-list))]))

(define (find-common-char-in-half string-line)
  (let* [(start-second (/ (string-length string-line) 2))
         (first-half (string->list string-line 0 start-second))
         (second-half (string->list string-line start-second))]

    (let loop-half [(half first-half)]
      (if (null? half)
        (error "No same char in line")

        (if (is-char-in-list (car half) second-half)
          (car half)
          (loop-half (cdr half)))))))

(call-with-input-file
  "3-1-input.txt"
  (lambda (port)
    (let loop [(acc 0)]
      (let [(line (read-line port))]
        (if (eof-object? line)
          (display acc)
          (loop (+ acc (char-value (find-common-char-in-half line)))))))))
