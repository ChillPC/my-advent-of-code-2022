(define (char-value char)
  (let [(ascii-value (char->integer char))]
    (cond
      [(and (<= 97 ascii-value) (<= ascii-value 122))
       (- ascii-value 96)]
      [(and (<= 65 ascii-value) (<= ascii-value 90))
       (- (+ 27 ascii-value) 65)]
      [else (error "Unwanted number")])))

(define (is-char-in-list char char-list)
  (cond
    [(null? char-list) #f]
    [(char=? char (car char-list)) #t]
    [else (is-char-in-list char (cdr char-list))]))

(define (find-common-char line-list1 line-list2 line-list3)
  (let loop [(line-list-origin line-list1)]
    (cond
      [(null? line-list-origin) (error "No same")]
      [(and (is-char-in-list (car line-list-origin) line-list2)
            (is-char-in-list (car line-list-origin) line-list3))
       (car line-list-origin)]
      [else (loop (cdr line-list-origin))])))

(call-with-input-file
  "3-2-input.txt"
  (lambda (port)
    (let loop [(acc 0)]
      (let [(line1 (read-line port))]
        (if (eof-object? line1)
          (display acc)
          (let [(line-list1 (string->list line1))
                (line-list2 (string->list (read-line port)))
                (line-list3 (string->list (read-line port)))]
            (loop (+ acc (char-value (find-common-char line-list1 line-list2 line-list3))))))))))
