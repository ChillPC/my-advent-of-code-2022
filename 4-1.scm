(load "irregex-0.9.3/irregex.scm")

(define re-pair (irregex "^(\\d+)-(\\d+),(\\d+)-(\\d+)$"))

(define (lutin-overlap lutin1 lutin2)
  (and (<= (car lutin1) (car lutin2))
       (<= (cdr lutin2) (cdr lutin1))))


(call-with-input-file
  "4-input.txt"
  (lambda (port)
    (let loop [(acc 0)]
      (let* [(line (read-line port))]
        (if (eof-object? line)
          (let* [(match (irregex-match re-pair line))
                 (lutin1 (cons (string->number (irregex-match-substring match 1))  
                               (string->number (irregex-match-substring match 2))))
                 (lutin2 (cons (string->number (irregex-match-substring match 3))  
                               (string->number (irregex-match-substring match 4))))]
            (if (or (lutin-overlap lutin1 lutin2) (lutin-overlap lutin2 lutin1))
              (loop (+ acc 1))
              (loop acc))))))))
