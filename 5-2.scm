(load "irregex-0.9.3/irregex.scm")

(define re (irregex "^move (\\d+) from (\\d+) to (\\d+)$"))

(define state
  #(
    (#\M #\S #\J #\L #\V #\F #\N #\R)
    (#\H #\W #\J #\F #\Z #\D #\N #\P)
    (#\G #\D #\C #\R #\W)
    (#\S #\B #\N)
    (#\N #\F #\B #\C #\P #\W #\Z #\M)
    (#\W #\M #\R #\P)
    (#\W #\S #\L #\G #\N #\T #\R)
    (#\V #\B #\N #\F #\H #\T #\Q)
    (#\F #\N #\Z #\H #\M #\L)))

(define (state-string-of-tops state)
  (list->string
    (reverse
      (let loop [(index 0)
                 (acc '())]
        (if (<= (vector-length state) index)
          acc
          (loop (+ 1 index)
                (cons (car (vector-ref state index))
                      acc)))))))

(call-with-input-file
  "5-input.txt"
  (lambda (port)
    (let loop []
      (let* [(line (read-line port))]
        (if (eof-object? line)
          (begin (display (state-string-of-tops state))
                 (newline))
          (let* [(match (irregex-match re line))
                 (how-many (string->number  (irregex-match-substring match 1)))
                 (index-from (- (string->number (irregex-match-substring match 2)) 1))
                 (index-to (- (string->number (irregex-match-substring match 3)) 1))
                 (stack-from (vector-ref state index-from))
                 (stack-to (vector-ref state index-to))]
            (vector-set! state index-to
                         (append (take stack-from how-many)
                                 stack-to))
            (vector-set! state index-from
                         (drop stack-from how-many))
            (loop)))))))
