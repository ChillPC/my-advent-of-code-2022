(define (split predicate? l)
  (let loop [(left l) (acc '())]
    (cond
      [(null? left) (values l '())]
      [(predicate? (car left))
       (values
         (reverse acc)
         left)]
      [else (loop (cdr left) (cons (car left) acc))])))


(define (index-start signal-string)
  (let [(signal (string->list signal-string))]
    (let loop [(signal signal)
               (acc '())
               (position-acc 1)]
      (let*-values [((last) (car signal))
                    ((to-keep to-throw)
                     (split (lambda (char) (char=? char last))
                            acc))
                    ((new-position) (+ position-acc (length to-throw)))]
        (cond
          [(null? signal) (raise "No more signal")]
          [(<= 13 (length to-keep)) position-acc] 
          [else (loop (cdr signal)
                      (cons last to-keep)
                      ;; new-position
                      (+ 1 position-acc))])))))

(call-with-input-file
   "6-input.txt"
   (lambda (port)
     (display
       (index-start
         (read-line port)))
     (newline)))




